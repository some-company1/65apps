//
//  ContactTableCell.swift
//  65Apps
//
//  Created by Антон on 26.09.2021.
//

import UIKit

final class ContctTableCell: UITableViewCell {
    static let identifier = String(describing: ContctTableCell.self)
    private var fullname: UILabel!
    private var telephone: UILabel!
    private var eMail: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: Student) {
        fullname.text = "\(model.name) \(model.lastName)"
        telephone.text = model.phone
        eMail.text = model.email
    }
}

private extension ContctTableCell {
    func setupUI() {
        selectionStyle = .none
        
        fullname = UILabel()
        fullname.numberOfLines = 0
        fullname.translatesAutoresizingMaskIntoConstraints = false
        fullname.font = .systemFont(ofSize: 20, weight: .bold)
        addSubview(fullname)
        
        telephone = UILabel()
        telephone.textColor = .gray
        telephone.translatesAutoresizingMaskIntoConstraints = false
        telephone.font = .systemFont(ofSize: 15, weight: .regular)
        addSubview(telephone)
        
        eMail = UILabel()
        eMail.textColor = .gray
        eMail.translatesAutoresizingMaskIntoConstraints = false
        eMail.font = .systemFont(ofSize: 15, weight: .regular)
        addSubview(eMail)
        
        NSLayoutConstraint.activate([
            fullname.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            fullname.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            fullname.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            
            telephone.topAnchor.constraint(equalTo: fullname.bottomAnchor, constant: 5),
            telephone.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            telephone.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            
            eMail.topAnchor.constraint(equalTo: telephone.bottomAnchor, constant: 5),
            eMail.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            eMail.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            eMail.rightAnchor.constraint(equalTo: rightAnchor, constant: -10)
        ])
    }
}

