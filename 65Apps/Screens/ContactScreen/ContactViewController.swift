//
//  ContactViewController.swift
//  65Apps
//
//  Created by Антон on 26.09.2021.
//

import UIKit

final class ContactViewController: UIViewController {
    private var searchBar: UISearchBar!
    private var tableView: UITableView!
    private var createStudentButton: UIButton!
    private var students: [Student] = []
    private var sourceStudents: [Student] = []
    private let studentUserDefaultsKey = "studentUserDefaultsKey"
    
    private lazy var saveHandler: (Student) -> Void = { [unowned self] student in
        self.updateStudents(with: student)
        self.saveStudents()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Контакты"
        setupUI()
        if let studentsData = UserDefaults.standard.value(forKey: studentUserDefaultsKey) as? Data,
           let students = try? JSONDecoder().decode([Student].self, from: studentsData) {
            self.students = students
            self.sourceStudents = students
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createStudentButton.center = CGPoint(x: view.bounds.maxX - 70,
                                             y: view.bounds.maxY - 70)
    }
}

//MARK: UITableViewDataSource
extension ContactViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContctTableCell.identifier, for: indexPath) as! ContctTableCell
        cell.configure(model: students[indexPath.row])
        return cell
    }
}

//MARK: UITableViewDelegate
extension ContactViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = EditStudentViewController()
        vc.configure(with: students[indexPath.row])
        vc.onSaveStudent = saveHandler
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let user = students[indexPath.row]
        if let index = sourceStudents.firstIndex(where: { $0.id == user.id }) {
            sourceStudents.remove(at: index)
        }
        
        students.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
        saveStudents()
    }
}

extension ContactViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text, text.isEmpty == false else {
            students = sourceStudents
            tableView.reloadData()
            return
        }
        students = sourceStudents.filter {
            $0.name.contains(text) || $0.lastName.contains(text) || $0.email.contains(text) || $0.phone.contains(text)
        }
        tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        students = sourceStudents
        tableView.reloadData()
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = nil
        view.endEditing(true)
    }
}

//MARK: private extension
private extension ContactViewController {
    func updateStudents(with student: Student) {
        if let index = students.firstIndex(where: { $0.id == student.id }) {
            students[index] = student
        } else {
            sourceStudents.append(student)
            students.append(student)
        }
        tableView.reloadData()
    }
    
    func saveStudents() {
        guard let data = try? JSONEncoder().encode(self.students) else { return }
        UserDefaults.standard.set(data, forKey: self.studentUserDefaultsKey)
    }
    
    @objc func addNewStudentAction() {
        let vc = EditStudentViewController()
        vc.onSaveStudent = saveHandler
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI() {
        view.backgroundColor = .white
        searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView()
        tableView.register(ContctTableCell.self, forCellReuseIdentifier: ContctTableCell.identifier)
        view.addSubview(tableView)
        view.addSubview(searchBar)
        
        createStudentButton = UIButton()
        createStudentButton.addTarget(self, action: #selector(addNewStudentAction), for: .touchUpInside)
        createStudentButton.backgroundColor = .black
        createStudentButton.layer.cornerRadius = 25
        createStudentButton.clipsToBounds = true
        createStudentButton.setTitle("✚", for: .normal)
        createStudentButton.titleLabel?.font = .systemFont(ofSize: 30)
        createStudentButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        view.addSubview(createStudentButton)
        
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            searchBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            searchBar.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -20),
            
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
