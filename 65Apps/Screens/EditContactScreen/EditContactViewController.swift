//
//  EditContactViewController.swift
//  65Apps
//
//  Created by Антон on 26.09.2021.
//

import UIKit

final class EditStudentViewController: UIViewController {
    var onSaveStudent: ((Student) -> Void)?
    private var nameEditTextField: UITextField!
    private var lastNameEditTextField: UITextField!
    private var companyTextField: UITextField!
    private var emailTextField: UITextField!
    private var phoneTextField: UITextField!
    private var doneButton: UIButton!
    
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)
    private var student: Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameEditTextField.text = student?.name
        lastNameEditTextField.text = student?.lastName
        emailTextField.text = student?.email
        phoneTextField.text = student?.phone
        companyTextField.text = student?.company
    }
    
    func configure(with student: Student) {
        self.student = student
    }
}

//MARK: UITextFieldDelegate
extension EditStudentViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField === phoneTextField {
            let fullString = (textField.text ?? "") + string
            textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
            return false
        }
        return true
    }
}

//MARK: private extension
private extension EditStudentViewController {
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else { return "+" }
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > 11 {
            let maxIndex = number.index(number.startIndex, offsetBy: 11)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex, offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        } else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        
        return "+" + number
    }
    
    @objc func doneAction() {
        if nameEditTextField.text?.isEmpty == false,
           lastNameEditTextField.text?.isEmpty == false,
           companyTextField.text?.isEmpty == false,
           phoneTextField.text?.isEmpty == false,
           emailTextField.text?.isEmpty == false {
            if isValidEmail(emailTextField.text!) {
                let student: Student
                if var st = self.student {
                    st.lastName = lastNameEditTextField.text!
                    st.name = nameEditTextField.text!
                    st.phone = phoneTextField.text!
                    st.email = emailTextField.text!
                    st.company = companyTextField.text!
                    student = st
                } else {
                    student = Student(name: nameEditTextField.text!,
                                      lastName: lastNameEditTextField.text!,
                                      email: emailTextField.text!,
                                      phone: phoneTextField.text!,
                                      company: companyTextField.text!)
                }
                onSaveStudent?(student)
                navigationController?.popViewController(animated: true)
            } else {
                showAlert(message: "Емайл введен неверно")
            }
        } else {
            showAlert(message: "Все поля должны быть заполнены")
        }
    }
    
    func setupUI() {
        view.backgroundColor = .white
        
        nameEditTextField = UITextField()
        nameEditTextField.placeholder = "Укажите имя"
        nameEditTextField.translatesAutoresizingMaskIntoConstraints = false
        nameEditTextField.keyboardType = .alphabet
        nameEditTextField.textAlignment = .center
        nameEditTextField.delegate = self
        nameEditTextField.layer.borderWidth = 1
        nameEditTextField.layer.borderColor = UIColor.lightGray.cgColor
        nameEditTextField.layer.cornerRadius = 5
        nameEditTextField.clipsToBounds = true
        view.addSubview(nameEditTextField)
        
        lastNameEditTextField = UITextField()
        lastNameEditTextField.translatesAutoresizingMaskIntoConstraints = false
        lastNameEditTextField.keyboardType = .alphabet
        lastNameEditTextField.delegate = self
        lastNameEditTextField.textAlignment = .center
        lastNameEditTextField.placeholder = "Укажите фамилию"
        lastNameEditTextField.layer.borderWidth = 1
        lastNameEditTextField.layer.borderColor = UIColor.lightGray.cgColor
        lastNameEditTextField.layer.cornerRadius = 5
        lastNameEditTextField.clipsToBounds = true
        view.addSubview(lastNameEditTextField)
        
        companyTextField = UITextField()
        companyTextField.placeholder = "Укажите компанию"
        companyTextField.translatesAutoresizingMaskIntoConstraints = false
        companyTextField.keyboardType = .alphabet
        companyTextField.textAlignment = .center
        companyTextField.delegate = self
        companyTextField.layer.borderWidth = 1
        companyTextField.layer.borderColor = UIColor.lightGray.cgColor
        companyTextField.layer.cornerRadius = 5
        companyTextField.clipsToBounds = true
        view.addSubview(companyTextField)
        
        emailTextField = UITextField()
        emailTextField.translatesAutoresizingMaskIntoConstraints = false
        emailTextField.keyboardType = .alphabet
        emailTextField.delegate = self
        emailTextField.textAlignment = .center
        emailTextField.placeholder = "Укажите e-mail"
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.cornerRadius = 5
        emailTextField.clipsToBounds = true
        view.addSubview(emailTextField)
        
        phoneTextField = UITextField()
        phoneTextField.translatesAutoresizingMaskIntoConstraints = false
        phoneTextField.keyboardType = .numberPad
        phoneTextField.delegate = self
        phoneTextField.textAlignment = .center
        phoneTextField.placeholder = "Укажите телефон"
        phoneTextField.layer.borderWidth = 1
        phoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        phoneTextField.layer.cornerRadius = 5
        phoneTextField.clipsToBounds = true
        view.addSubview(phoneTextField)
        
        doneButton = UIButton()
        doneButton.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.setTitle("Готово 👍", for: .normal)
        doneButton.setTitleColor(.black, for: .normal)
        doneButton.layer.borderWidth = 1
        doneButton.layer.borderColor = UIColor.lightGray.cgColor
        doneButton.layer.cornerRadius = 10
        doneButton.clipsToBounds = true
        view.addSubview(doneButton)
    }
    
    func setupLayout() {
        NSLayoutConstraint.activate([
            nameEditTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            nameEditTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            nameEditTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            
            lastNameEditTextField.topAnchor.constraint(equalTo: nameEditTextField.topAnchor, constant: 50),
            lastNameEditTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            lastNameEditTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            
            companyTextField.topAnchor.constraint(equalTo: lastNameEditTextField.topAnchor, constant: 50),
            companyTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            companyTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            
            emailTextField.topAnchor.constraint(equalTo: companyTextField.topAnchor, constant: 50),
            emailTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            emailTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            
            phoneTextField.topAnchor.constraint(equalTo: emailTextField.topAnchor, constant: 50),
            phoneTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            phoneTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            
            doneButton.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 30),
            doneButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            doneButton.widthAnchor.constraint(equalToConstant: 100)
        ])
    }
}
