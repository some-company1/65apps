//
//  ContactModel.swift
//  65Apps
//
//  Created by Антон on 26.09.2021.
//

import Foundation

struct Student: Codable {
    var name: String
    var lastName: String
    var email: String
    var phone: String
    var company: String
    
    private(set) var id = UUID().uuidString
}
